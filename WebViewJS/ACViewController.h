//
//  ACViewController.h
//  WebViewJS
//
//  Created by Ankur on 22/01/15.
//  Copyright (c) 2015 my.company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
