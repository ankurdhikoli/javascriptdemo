//
//  ACViewController.m
//  WebViewJS
//
//  Created by Ankur on 22/01/15.
//  Copyright (c) 2015 my.company. All rights reserved.
//

#import "ACViewController.h"

@interface ACViewController ()

@end

@implementation ACViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"www"]];
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden=NO;

}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBarHidden=YES;


}


- (void) webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"webviewDidFinished");
    

    
    
    
}
- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    
    
    
    if (navigationType == UIWebViewNavigationTypeFormSubmitted) {
        // form is submitted or button is clicked , perform your actions here
        NSLog(@" UIWebViewNavigationTypeFormSubmitted ");
    }
    
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        // form is submitted or button is clicked , perform your actions here
        NSLog(@" UIWebViewNavigationTypeLinkClicked ");
    }
    
    if (navigationType == UIWebViewNavigationTypeBackForward) {
        // form is submitted or button is clicked , perform your actions here
        NSLog(@" UIWebViewNavigationTypeBackForward ");
    }
    
    if (navigationType == UIWebViewNavigationTypeReload) {
        // form is submitted or button is clicked , perform your actions here
        NSLog(@" UIWebViewNavigationTypeReload ");
    }
    
    if (navigationType == UIWebViewNavigationTypeFormResubmitted) {
        // form is submitted or button is clicked , perform your actions here
        NSLog(@" UIWebViewNavigationTypeFormResubmitted ");
    }
    
    if (navigationType == UIWebViewNavigationTypeOther) {
        // form is submitted or button is clicked , perform your actions here
        NSLog(@" UIWebViewNavigationTypeOther ");
    }
    
    
    return YES;
    
}


@end
