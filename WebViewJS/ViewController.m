/*
 Copyright (c) 2014 Cullen SUN
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


//
//  ViewController.m
//  WebViewTest
//
//  Created by Cullen Sun on 12/4/13.
//

#import "ViewController.h"
#import "ACViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    scale=100;
    self.navigationController.navigationBarHidden=YES;
    currentFontLbl.text=[NSString stringWithFormat:@"%d%%",scale];
    
     NSString * path=[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"];
    NSString * theS=[NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSURL *base=[NSURL URLWithString:@"www.ios-local.com"];
    [_webView loadHTMLString:theS baseURL:base];
    
    
//    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"www"]];
//    
//    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
    
}



- (void) webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"webviewDidFinished");
    _webView.delegate=self;

    NSString * jsCountString=@"myMultiply(2,2);";
    NSString * returnCountS=[_webView stringByEvaluatingJavaScriptFromString:jsCountString];
    NSLog(@"myMultiply %@",returnCountS);
    
    
    
}
- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    
    
    
    
    
    if (navigationType == UIWebViewNavigationTypeFormSubmitted) {
        // form is submitted or button is clicked , perform your actions here
        NSLog(@" UIWebViewNavigationTypeFormSubmitted ");
    }
    
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        // form is submitted or button is clicked , perform your actions here
        NSLog(@" UIWebViewNavigationTypeLinkClicked ");
    }
    
    if (navigationType == UIWebViewNavigationTypeBackForward) {
        // form is submitted or button is clicked , perform your actions here
        NSLog(@" UIWebViewNavigationTypeBackForward ");
    }
    
    if (navigationType == UIWebViewNavigationTypeReload) {
        // form is submitted or button is clicked , perform your actions here
        NSLog(@" UIWebViewNavigationTypeReload ");
    }
    
    if (navigationType == UIWebViewNavigationTypeFormResubmitted) {
        // form is submitted or button is clicked , perform your actions here
        NSLog(@" UIWebViewNavigationTypeFormResubmitted ");
    }
    
    if (navigationType == UIWebViewNavigationTypeOther) {
        // form is submitted or button is clicked , perform your actions here
        NSLog(@" UIWebViewNavigationTypeOther ");
    }

    
    if ([[request.URL scheme] isEqual:@"yourapp"]) {
        if ([[request.URL path] isEqual:@"buttonClicked"]) {
            NSLog(@"Evry thing ok we can get callback button from html code");
        }
        return NO; //
    }else {
        return YES; // Tells the webView to go ahead and load the URL
    }
    
    
    return YES;
    
}


- (IBAction)nextView1BtnClicked:(id)sender {
    
    
    ACViewController* viewController = [[ACViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)fontSizeChange:(UISlider*)sender{
    currentFontLbl.text=[NSString stringWithFormat:@"%0.f%%",sender.value];
    NSString * jsString=[NSString stringWithFormat:@"document.getElementById('pageWrap').style.webkitTextSizeAdjust='%@';", currentFontLbl.text];
   [_webView stringByEvaluatingJavaScriptFromString:jsString];
}

- (IBAction)showAlert:(id)sender{
    NSString * jsString=@"testing();";
   [_webView stringByEvaluatingJavaScriptFromString:jsString];
}

- (IBAction)clickHtmlBtn:(UIButton *)sender {
    
    NSString *jsStat = @"document.getElementsByName('ankbtn')[0].click()";
    [_webView stringByEvaluatingJavaScriptFromString:jsStat];
    
   // [_webView reload];
   // [_webView stopLoading];
   // [_webView goBack];
   // [_webView goForward];


}

- (IBAction)getjsFunctionReturn:(id)sender{
    NSString * jsString=@"getSum(4);";
    NSString * returnS=[_webView stringByEvaluatingJavaScriptFromString:jsString];
    NSLog(@"getsum %@",returnS);
    
    
    
//    NSString * jsCountString=@"countit(ankurcount);";
//    NSString * returnCountS=[theWeb stringByEvaluatingJavaScriptFromString:jsCountString];
//    NSLog(@"counting %@",returnCountS);
    
}
@end
